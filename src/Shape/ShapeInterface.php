<?php

declare(strict_types=1);

namespace Wrd\SpiralTable\Shape;

use Wrd\SpiralTable\Shape\Point\PointInterface;

interface ShapeInterface
{
    public function isPointInBorder(PointInterface $point): bool;

    public function isPointInside(PointInterface $point): bool;
}
