<?php

declare(strict_types=1);

namespace Wrd\SpiralTable\Shape\Polygon;

use Wrd\SpiralTable\Shape\Line\Line;
use Wrd\SpiralTable\Shape\Line\LineInterface;
use Wrd\SpiralTable\Shape\Point\PointInterface;
use Wrd\SpiralTable\Shape\ShapeInterface;

final class Triangle implements ShapeInterface
{
    private LineInterface $abLine;
    private LineInterface $acLine;
    private PointInterface $pointA;
    private PointInterface $pointB;
    private PointInterface $pointC;

    public function __construct(PointInterface $pointA, PointInterface $pointB, PointInterface $pointC)
    {
        $this->pointA = $pointA;
        $this->pointB = $pointB;
        $this->pointC = $pointC;

        $this->abLine = new Line($pointA, $pointB);
        $this->acLine = new Line($pointA, $pointC);
    }

    public function isPointInBorder(PointInterface $point): bool
    {
        return $this->acLine->isPointBelongsToSegment($point) || $this->abLine->isPointBelongsToSegment($point);
    }

    public function isPointInside(PointInterface $point): bool
    {
        if (
            $this->pointA->isSameAs($this->pointB)
            || $this->pointB->isSameAs($this->pointC)
            || $this->pointC->isSameAs($this->pointA)
        ) {
            return false;
        }

        $xB = $this->pointB->getX() - $this->pointA->getX();
        $yB = $this->pointB->getY() - $this->pointA->getY();

        $xC = $this->pointC->getX() - $this->pointA->getX();
        $yC = $this->pointC->getY() - $this->pointA->getY();

        $x = $point->getX() - $this->pointA->getX();
        $y = $point->getY() - $this->pointA->getY();

        $maxRangeValue = $xB * $yC - $xC * $yB;
        $val1 = $x * ($yB - $yC) + $y * ($xC - $xB) + $xB * $yC - $xC * $yB;
        $val2 = $x * $yC - $y * $xC;
        $val3 = $y * $xB - $x * $yB;

        return ($val1 >= 0 && $val1 <= $maxRangeValue)
            && ($val2 >= 0 && $val2 <= $maxRangeValue)
            && ($val3 >= 0 && $val3 <= $maxRangeValue);
    }
}
