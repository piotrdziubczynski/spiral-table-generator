<?php

declare(strict_types=1);

namespace Wrd\SpiralTable\Shape\Line;

use Wrd\SpiralTable\Shape\Point\PointInterface;

final class Line implements LineInterface
{
    private PointInterface $pointA;
    private PointInterface $pointB;

    public function __construct(PointInterface $pointA, PointInterface $pointB)
    {
        $this->pointA = $pointA;
        $this->pointB = $pointB;
    }

    public function isPointBelongsToSegment(PointInterface $point): bool
    {
        if ($this->pointA->isSameAs($this->pointB) || !$this->isPointBelongsToLine($point)) {
            return false;
        }

        return (
                $point->getX() >= min($this->pointA->getX(), $this->pointB->getX())
                && $point->getX() <= max($this->pointA->getX(), $this->pointB->getX())
            )
            && (
                $point->getY() >= min($this->pointA->getY(), $this->pointB->getY())
                && $point->getY() <= max($this->pointA->getY(), $this->pointB->getY())
            );
    }

    public function isPointBelongsToLine(PointInterface $point): bool
    {
        $partOne = $this->pointB->getX() * $point->getY() +
            $this->pointA->getX() * $this->pointB->getY() +
            $point->getX() * $this->pointA->getY();

        $partTwo = $this->pointA->getX() * $point->getY() +
            $this->pointB->getX() * $this->pointA->getY() +
            $point->getX() * $this->pointB->getY();

        return ($partOne - $partTwo) === 0;
    }
}
