<?php

declare(strict_types=1);

namespace Wrd\SpiralTable\Shape\Line;

use Wrd\SpiralTable\Shape\Point\PointInterface;

interface LineInterface
{
    public function isPointBelongsToSegment(PointInterface $point): bool;

    public function isPointBelongsToLine(PointInterface $point): bool;
}
