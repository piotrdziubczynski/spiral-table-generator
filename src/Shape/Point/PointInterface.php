<?php

declare(strict_types=1);

namespace Wrd\SpiralTable\Shape\Point;

interface PointInterface
{
    public function getX(): int;

    public function getY(): int;

    public function increaseX(): void;

    public function decreaseX(): void;

    public function increaseY(): void;

    public function decreaseY(): void;

    public function isSameAs(PointInterface $point): bool;
}
