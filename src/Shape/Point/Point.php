<?php

declare(strict_types=1);

namespace Wrd\SpiralTable\Shape\Point;

final class Point implements PointInterface
{
    private int $x;
    private int $y;

    public function __construct(int $x, int $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function getX(): int
    {
        return $this->x;
    }

    public function getY(): int
    {
        return $this->y;
    }

    public function increaseX(): void
    {
        ++$this->x;
    }

    public function decreaseX(): void
    {
        --$this->x;
    }

    public function increaseY(): void
    {
        ++$this->y;
    }

    public function decreaseY(): void
    {
        --$this->y;
    }

    public function isSameAs(PointInterface $point): bool
    {
        return $this->x === $point->getX() && $this->y === $point->getY();
    }
}
