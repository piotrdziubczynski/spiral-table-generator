<?php

declare(strict_types=1);

namespace Wrd\SpiralTable\Table;

interface TableInterface
{
    public function getTable(): ?array;

    public function displayTable(): string;
}
