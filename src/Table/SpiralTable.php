<?php

declare(strict_types=1);

namespace Wrd\SpiralTable\Table;

use LogicException;
use RuntimeException;
use Wrd\SpiralTable\Common\GeneratorInterface;
use Wrd\SpiralTable\Shape\Point\Point;
use Wrd\SpiralTable\Shape\Point\PointInterface;
use Wrd\SpiralTable\Table\Area\Factory\TableAreaFactory;
use Wrd\SpiralTable\Table\Area\TableAreaInterface;

final class SpiralTable implements TableInterface, GeneratorInterface
{
    private TableAreaFactory $areaFactory;
    private PointInterface $center;
    private int $columns;
    private int $end;
    private string $numberFormat;
    private int $rows;
    private ?array $table;

    public function __construct(int $end)
    {
        if ($end < 1) {
            throw new LogicException('The last element should be a natural number greater than ZERO');
        }

        $this->end = $end;
        $this->table = null;

        $this->createNumberFormat();
        $this->calculateDimensions();
        $this->calculateCenter();

        $this->areaFactory = new TableAreaFactory($this->columns, $this->rows, $this->center);
    }

    private function createNumberFormat(): void
    {
        $leadingZeros = 2;
        $endLength = strlen((string)$this->end);

        if ($endLength > $leadingZeros) {
            $leadingZeros = $endLength;
        }

        $this->numberFormat = sprintf('%%0%dd', $leadingZeros);
    }

    private function calculateDimensions(): void
    {
        $sqrt = $this->end ** 0.5;
        $diff = $sqrt - (floor($sqrt) ?: 0);
        $isSquare = 0.0 === $diff;
        $columns = $rows = (int)(floor($sqrt) ?: 0);

        if (!$isSquare) {
            ++$columns; // [0.0; 0.5) - add column

            if (1.0 === round($diff)) {
                ++$rows; // [0.5; 1.0] - add row
            }
        }

        $this->columns = $columns;
        $this->rows = $rows;
    }

    private function calculateCenter(): void
    {
        $x = (int)((ceil($this->columns / 2) ?: 0) - 1);
        $y = (int)((ceil($this->rows / 2) ?: 0) - 1);

        if ($x < 0 || $y < 0) {
            throw new RuntimeException('Something went wrong. Please contact Support.');
        }

        $this->center = new Point($x, $y);
    }

    public function getTable(): ?array
    {
        return $this->table;
    }

    public function displayTable(): string
    {
        if (null === $this->table) {
            return '';
        }

        $table = array_map(
            function (array $row) {
                return implode(' | ', $row);
            },
            $this->table
        );

        return implode(PHP_EOL, $table);
    }

    public function generate(): void
    {
        if (1 === $this->end) {
            $this->table = [[sprintf($this->numberFormat, 1)]];

            return;
        }

        $this->fill($this->areaFactory->getDefaultArea());
    }

    private function fill(TableAreaInterface $area): void
    {
        $this->table = array_fill(0, $this->rows, array_fill(0, $this->columns, null));
        $point = clone $this->center;
        $number = 1;

        do {
            if (null !== $this->table[$point->getY()][$point->getX()]) {
                $area->fixPoint($point);
            }

            $this->table[$point->getY()][$point->getX()] = sprintf($this->numberFormat, $number++);

            $area->calculatePoint($point);

            $area = $this->areaFactory->getArea($point, $area);
        } while ($number <= $this->end);
    }
}
