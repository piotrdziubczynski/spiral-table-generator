<?php

declare(strict_types=1);

namespace Wrd\SpiralTable\Table\Area;

use Wrd\SpiralTable\Shape\Point\PointInterface;

interface TableAreaInterface
{
    public function isPointBelongs(PointInterface $point): bool;

    public function calculatePoint(PointInterface $point): void;

    public function fixPoint(PointInterface $point): void;
}
