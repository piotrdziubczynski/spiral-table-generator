<?php

declare(strict_types=1);

namespace Wrd\SpiralTable\Table\Area;

use Wrd\SpiralTable\Shape\Point\PointInterface;

final class WestTableArea extends AbstractTableArea
{
    public function calculatePoint(PointInterface $point): void
    {
        $point->decreaseY();
    }

    public function fixPoint(PointInterface $point): void
    {
        $point->decreaseX();
        $point->increaseY();
    }
}
