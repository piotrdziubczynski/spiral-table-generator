<?php

declare(strict_types=1);

namespace Wrd\SpiralTable\Table\Area;

use Wrd\SpiralTable\Shape\Point\PointInterface;

final class EastTableArea extends AbstractTableArea
{
    public function calculatePoint(PointInterface $point): void
    {
        $point->increaseY();
    }

    public function fixPoint(PointInterface $point): void
    {
        $point->increaseX();
        $point->decreaseY();
    }
}
