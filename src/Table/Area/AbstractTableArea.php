<?php

declare(strict_types=1);

namespace Wrd\SpiralTable\Table\Area;

use Wrd\SpiralTable\Shape\Point\PointInterface;
use Wrd\SpiralTable\Shape\ShapeInterface;

abstract class AbstractTableArea implements TableAreaInterface
{
    private ShapeInterface $shape;

    public function __construct(ShapeInterface $shape)
    {
        $this->shape = $shape;
    }

    public function isPointBelongs(PointInterface $point): bool
    {
        return $this->shape->isPointInBorder($point) || $this->shape->isPointInside($point);
    }
}
