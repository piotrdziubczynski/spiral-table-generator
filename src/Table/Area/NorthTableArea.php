<?php

declare(strict_types=1);

namespace Wrd\SpiralTable\Table\Area;

use Wrd\SpiralTable\Shape\Point\PointInterface;

final class NorthTableArea extends AbstractTableArea
{
    public function calculatePoint(PointInterface $point): void
    {
        $point->increaseX();
    }

    public function fixPoint(PointInterface $point): void
    {
        $point->decreaseX();
        $point->decreaseY();
    }
}
