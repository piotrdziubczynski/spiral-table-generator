<?php

declare(strict_types=1);

namespace Wrd\SpiralTable\Table\Area\Factory;

use Wrd\SpiralTable\Shape\Point\Point;
use Wrd\SpiralTable\Shape\Point\PointInterface;
use Wrd\SpiralTable\Shape\Polygon\Triangle;
use Wrd\SpiralTable\Table\Area\EastTableArea;
use Wrd\SpiralTable\Table\Area\NorthTableArea;
use Wrd\SpiralTable\Table\Area\SouthTableArea;
use Wrd\SpiralTable\Table\Area\TableAreaInterface;
use Wrd\SpiralTable\Table\Area\WestTableArea;

final class TableAreaFactory
{
    private TableAreaInterface $northArea;
    private TableAreaInterface $southArea;
    private TableAreaInterface $eastArea;
    private TableAreaInterface $westArea;

    public function __construct(int $columns, int $rows, PointInterface $center)
    {
        $topLeftPoint = new Point(0, 0);
        $topRightPoint = new Point($columns - 1, 0);
        $bottomRightPoint = new Point($columns - 1, $rows - 1);
        $bottomLeftPoint = new Point(0, $rows - 1);

        $northFixedPoint = clone $topRightPoint;
        $northFixedPoint->decreaseX();

        $eastFixedPoint = clone $bottomRightPoint;
        $eastFixedPoint->decreaseY();

        $southFixedPoint = clone $bottomLeftPoint;
        $southFixedPoint->increaseX();

        $westFixedPoint = clone $topLeftPoint;
        $westFixedPoint->increaseY();

        $this->northArea = new NorthTableArea(new Triangle($topLeftPoint, $northFixedPoint, $center));
        $this->eastArea = new EastTableArea(new Triangle($topRightPoint, $eastFixedPoint, $center));
        $this->southArea = new SouthTableArea(new Triangle($bottomRightPoint, $southFixedPoint, $center));
        $this->westArea = new WestTableArea(new Triangle($bottomLeftPoint, $westFixedPoint, $center));
    }

    public function getDefaultArea(): TableAreaInterface
    {
        return $this->northArea;
    }

    public function getArea(PointInterface $point, TableAreaInterface $area): TableAreaInterface
    {
        if ($this->eastArea->isPointBelongs($point)) {
            $area = $this->eastArea;
        } elseif ($this->southArea->isPointBelongs($point)) {
            $area = $this->southArea;
        } elseif ($this->westArea->isPointBelongs($point)) {
            $area = $this->westArea;
        } elseif ($this->northArea->isPointBelongs($point)) {
            $area = $this->northArea;
        }

        return $area;
    }
}
