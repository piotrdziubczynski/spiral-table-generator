<?php

declare(strict_types=1);

namespace Wrd\SpiralTable\Common;

interface GeneratorInterface
{
    public function generate(): void;
}
