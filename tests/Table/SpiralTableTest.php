<?php

declare(strict_types=1);

namespace Wrd\SpiralTable\Tests\Table;

use LogicException;
use PHPUnit\Framework\TestCase;
use Wrd\SpiralTable\Table\SpiralTable;

final class SpiralTableTest extends TestCase
{
    public function testCannotBeCreatedFromInvalidSize(): void
    {
        $this->expectException(LogicException::class);

        new SpiralTable(0);
    }

    public function testWillReturnNullWithoutGenerateMethod(): void
    {
        $spiralTable = new SpiralTable(1);

        $this->assertSame(null, $spiralTable->getTable());
    }

    public function testWillReturnCorrect1x1Array(): void
    {
        $spiralTable = new SpiralTable(1);
        $spiralTable->generate();

        $array = $spiralTable->getTable();

        $this->assertSameSize([['01']], $array);
        $this->assertSame('01', $array[0][0]);
    }

    public function testWillReturn2x3Array(): void
    {
        $spiralTable = new SpiralTable(5);
        $spiralTable->generate();

        $array = $spiralTable->getTable();

        $this->assertSame(
            [
                [null, '01', '02'],
                ['05', '04', '03'],
            ],
            $array
        );
        $this->assertSame(null, $array[0][0]);
        $this->assertSame('01', $array[0][1]);
        $this->assertSame('05', $array[1][0]);
    }

    public function testWillReturn3x3Array(): void
    {
        $spiralTable = new SpiralTable(7);
        $spiralTable->generate();

        $array = $spiralTable->getTable();

        $this->assertSame(
            [
                ['07', null, null],
                ['06', '01', '02'],
                ['05', '04', '03'],
            ],
            $array
        );
        $this->assertSame('07', $array[0][0]);
        $this->assertSame(null, $array[0][1]);
        $this->assertSame('01', $array[1][1]);
    }

    public function testWillReturn3x4Array(): void
    {
        $spiralTable = new SpiralTable(10);
        $spiralTable->generate();

        $array = $spiralTable->getTable();

        $this->assertSame(
            [
                ['07', '08', '09', '10'],
                ['06', '01', '02', null],
                ['05', '04', '03', null],
            ],
            $array
        );
        $this->assertSame('10', $array[0][3]);
        $this->assertSame(null, $array[1][3]);
        $this->assertSame('01', $array[1][1]);
    }

    public function testWillReturn4x4Array(): void
    {
        $spiralTable = new SpiralTable(13);
        $spiralTable->generate();

        $array = $spiralTable->getTable();

        $this->assertSame(
            [
                ['07', '08', '09', '10'],
                ['06', '01', '02', '11'],
                ['05', '04', '03', '12'],
                [null, null, null, '13'],
            ],
            $array
        );
        $this->assertSame('13', $array[3][3]);
        $this->assertSame(null, $array[3][2]);
        $this->assertSame('01', $array[1][1]);
    }

    public function testWillReturn4x5Array(): void
    {
        $spiralTable = new SpiralTable(17);
        $spiralTable->generate();

        $array = $spiralTable->getTable();

        $this->assertSame(
            [
                [null, '07', '08', '09', '10'],
                [null, '06', '01', '02', '11'],
                [null, '05', '04', '03', '12'],
                ['17', '16', '15', '14', '13'],
            ],
            $array
        );
        $this->assertSame('17', $array[3][0]);
        $this->assertSame(null, $array[2][0]);
        $this->assertSame('01', $array[1][2]);
    }

    public function testWillReturn5x5Array(): void
    {
        $spiralTable = new SpiralTable(21);
        $spiralTable->generate();

        $array = $spiralTable->getTable();

        $this->assertSame(
            [
                ['21', null, null, null, null],
                ['20', '07', '08', '09', '10'],
                ['19', '06', '01', '02', '11'],
                ['18', '05', '04', '03', '12'],
                ['17', '16', '15', '14', '13'],
            ],
            $array
        );
        $this->assertSame('21', $array[0][0]);
        $this->assertSame(null, $array[0][1]);
        $this->assertSame('01', $array[2][2]);
    }

    public function testWillReturn5x6Array(): void
    {
        $spiralTable = new SpiralTable(26);
        $spiralTable->generate();

        $array = $spiralTable->getTable();

        $this->assertSame(
            [
                ['21', '22', '23', '24', '25', '26'],
                ['20', '07', '08', '09', '10', null],
                ['19', '06', '01', '02', '11', null],
                ['18', '05', '04', '03', '12', null],
                ['17', '16', '15', '14', '13', null],
            ],
            $array
        );
        $this->assertSame('26', $array[0][5]);
        $this->assertSame(null, $array[1][5]);
        $this->assertSame('01', $array[2][2]);
    }

    public function testWillReturn6x6Array(): void
    {
        $spiralTable = new SpiralTable(31);
        $spiralTable->generate();

        $array = $spiralTable->getTable();

        $this->assertSame(
            [
                ['21', '22', '23', '24', '25', '26'],
                ['20', '07', '08', '09', '10', '27'],
                ['19', '06', '01', '02', '11', '28'],
                ['18', '05', '04', '03', '12', '29'],
                ['17', '16', '15', '14', '13', '30'],
                [null, null, null, null, null, '31'],
            ],
            $array
        );
        $this->assertSame('31', $array[5][5]);
        $this->assertSame(null, $array[5][4]);
        $this->assertSame('01', $array[2][2]);
    }

    public function testWillReturn6x7Array(): void
    {
        $spiralTable = new SpiralTable(37);
        $spiralTable->generate();

        $array = $spiralTable->getTable();

        $this->assertSame(
            [
                [null, '21', '22', '23', '24', '25', '26'],
                [null, '20', '07', '08', '09', '10', '27'],
                [null, '19', '06', '01', '02', '11', '28'],
                [null, '18', '05', '04', '03', '12', '29'],
                [null, '17', '16', '15', '14', '13', '30'],
                ['37', '36', '35', '34', '33', '32', '31'],
            ],
            $array
        );
        $this->assertSame('37', $array[5][0]);
        $this->assertSame(null, $array[4][0]);
        $this->assertSame('01', $array[2][3]);
    }

    public function testWillReturn7x7Array(): void
    {
        $spiralTable = new SpiralTable(43);
        $spiralTable->generate();

        $array = $spiralTable->getTable();

        $this->assertSame(
            [
                ['43', null, null, null, null, null, null],
                ['42', '21', '22', '23', '24', '25', '26'],
                ['41', '20', '07', '08', '09', '10', '27'],
                ['40', '19', '06', '01', '02', '11', '28'],
                ['39', '18', '05', '04', '03', '12', '29'],
                ['38', '17', '16', '15', '14', '13', '30'],
                ['37', '36', '35', '34', '33', '32', '31'],
            ],
            $array
        );
        $this->assertSame('43', $array[0][0]);
        $this->assertSame(null, $array[0][1]);
        $this->assertSame('01', $array[3][3]);
    }
}
